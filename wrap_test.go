package wrap

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestWrap(t *testing.T) {
	tests := []struct {
		input    string
		width    int
		expected string
	}{
		{"", 1, ""},
		{" ", 1, " "},
		{"a", 1, "a"},
		{"ab", 1, "a\nb"},
		{"abc", 1, "a\nb\nc"},
		{"a b", 1, "a\nb"},
		{"a bc", 3, "a\nbc"},
		{"a b c d", 3, "a b\nc d"},
		{"a dog with a bone", 6, "a dog\nwith a\nbone"},
		{"four score and seven years ago our fathers brought forth upon this continent", 7,
			"four\nscore\nand\nseven\nyears\nago our\nfathers\nbrought\nforth\nupon\nthis\ncontine\nnt"},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("string '%s' with width %d", test.input, test.width), func(t *testing.T) {
			assert.Equal(t, test.expected, Wrap(test.input, test.width))
		})
	}
}

func Wrap(input string, width int) string {

	if len(input) <= width {
		return input
	}

	runes := []rune(input)
	breakPoint := strings.LastIndex(string(runes[0:width+1]), " ")

	if breakPoint == -1 {
		breakPoint = width
	}

	return string(runes[0:breakPoint]) + "\n" + Wrap(strings.TrimSpace(string(runes[breakPoint:])), width)
}
